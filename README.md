# Linux Hardening

Linux Hardening for Home Desktop and Gaming users - NOT designed for Server or Enterprise scenarios.

Please review my configuration file carefully before applying it. 


## Scenario

- ICMP echo messages are not harmful, we simply assume that our current network is not compromised.
- Security comes above performance. This does not exclude performance is not put in consideration but valued lower than security.
- You do not debug on the machine which means processes that depend on it might not be correctly function. I am aware that auditing the system is critical but we assume we can debug things trough external programs as well as systems.
- Your OS does not act as a router, which means this is not for Rb-Pi with a router OS installed on it.
- Your OS is on a network connection that is relatively stable, which means wired or wireless. Mobile connections are not put into consideration but in theory it should work on those connections too. 


## Goal

My goal is to present hardening that can easily applied onto all Linux Distros however, my focus is Arch and Manjaro which I use as my personal test ground to do several checks.

Some tweaks as well as hardening can individually applied others can blindly applied because they depend on other factors mentioned in-depth in the documentation.

Apply best practices and combining them it usability in an daily environment is essential.


## Review and not blindly imitate and apply everything

My goal is not to provide a script that you execute and then this is it, I believe you learn more when you do things yourself, I will provide you with an template but critical things must be reviewed by yourself because some things might break your daily driver because you are used to other standards and depend on other features.


## What is NOT my goal

- I am not paranoid, some things will be allowed otherwise the OS becomes useless. At the end of the day you want to work with the OS and not sit there and execute preinstalled apps, this is not security this, it is limiting abilities. I assume that you are secure at your home and not that people in a van with spy equipment are in front of your house.
- I leave things like Bluetooth enabled because I use it myself and I am not in any environment that this can be exploited. My goal is not to disable every single possible network protocol or interface and then claim this is security, it is not, because you can pretty much harden every OS so that it becomes unusable in an daily environment.
- Servers or Enterprise based systems.
- The configuration is not meant to be portable, means I do not take changes in your location into consideration. I assume you are at home and do not take your desktop or laptop system with you on a journey.


## Backups are mandatory

Do a backup before you apply anything here. In general you should do backups because even applying best practices and hardening can lead to malware infections, data corruption or cause partition issues which can prevent your OS from boot successfully.

I assume you do a backup once a week or whenever you do bigger changes onto your daily driver.


## Drawbacks

Like with every hardening there comes a downside and I will clearly outline them so that you get an first impression what might can happen.

- Performance. I value security as well as privacy over performance.
- Longer login times and decreased speed in the whole boot process.
- Possible Hibernation issues.
- Anti-Cheat might break or even worse, because of the changes you might get flagged and banned from the system. Some anti-cheat systems check specific kernel parameters and detect boot changes and because of this it can cause those mentioned issues.
- Some applications might not start or crash. I try to address this based on reports or feedback but I usually test the configuration before I push it or comment things out that might can cause severe problems with application compatibility.


## Documentation

Most parameters are well documented in the [Linux Kernel Documentation](https://www.kernel.org/doc/Documentation/sysctl/) which acts like my reference.
